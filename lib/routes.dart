import 'package:flutter/material.dart';
import 'package:flutter_routing/pages/detail.dart';
import 'package:flutter_routing/pages/fruits.dart';
import 'package:flutter_routing/pages/home.dart';
import 'package:flutter_routing/pages/shell_route.dart';
import 'package:go_router/go_router.dart';

final _rootNavigatorKey = GlobalKey<NavigatorState>();
final _sectionNavigatorKey = GlobalKey<NavigatorState>();

final router = GoRouter(
  initialLocation: '/',
  navigatorKey: _rootNavigatorKey,
  routes: [
    GoRoute(
      path: '/',
      name: 'home',
      builder: (context, state) => const HomePage(),
    ),
    GoRoute(
      path: '/details/:id',
      name: 'details',
      builder: (context, state) {
        final id = state.pathParameters['id'];
        return DetailPage(id: int.parse(id!));
      },
    ),
    GoRoute(
        path: '/fruits',
        builder: (context, state) => const FruitPage(),
        routes: <RouteBase>[
          GoRoute(
            path: 'fruits-details',
            builder: (context, state) => const FruitDetailPage(),
          )
        ]),
    StatefulShellRoute.indexedStack(
      builder: (context, state, navigationShell) {
        return ScaffoldWithNavbar(navigationShell);
      },
      branches: [
        StatefulShellBranch(
          navigatorKey: _sectionNavigatorKey,
          routes: <RouteBase>[
            GoRoute(
              path: '/feed',
              builder: (context, state) => const FeedPage(),
            ),
            GoRoute(
              path: '/feed/:id',
              builder: (context, state) {
                final id = state.pathParameters['id'];
                if (id == null) {
                  return const Placeholder();
                }
                return FeedDetailPage(int.parse(id));
              },
            )
          ],
        ),
        StatefulShellBranch(
          routes: <RouteBase>[
            GoRoute(
              path: '/shope',
              builder: (context, state) => const ShopePage(),
            )
          ],
        ),
      ],
    ),
  ],
);
