import 'package:flutter/material.dart';

class FruitPage extends StatelessWidget {
  const FruitPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text(
          'Fruit Page',
        ),
      ),
    );
  }
}

class FruitDetailPage extends StatelessWidget {
  const FruitDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text(
          'Fruit Detail Page',
        ),
      ),
    );
  }
}
