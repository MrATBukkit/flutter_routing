import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Expanded(
            child: Center(
              child: Text(
                'Home Page',
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              context.go('/details/1');
            },
            child: const Text('Go to Details'),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            onPressed: () {
              context.go('/fruits/fruits-details');
            },
            child: const Text('Go to Fruits details'),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            onPressed: () {
              context.go('/feed');
            },
            child: const Text('Go to Feed'),
          ),
          const SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }
}
