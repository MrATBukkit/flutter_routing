import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class ScaffoldWithNavbar extends StatelessWidget {
  const ScaffoldWithNavbar(this.navigationShell, {super.key});

  final StatefulNavigationShell navigationShell;

  void _onTap(index) {
    navigationShell.goBranch(
      index,
      initialLocation: index == navigationShell.currentIndex,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: navigationShell,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: navigationShell.currentIndex,
        onTap: _onTap,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shop),
            label: 'Shop',
          ),
        ],
      ),
    );
  }
}

class FeedPage extends StatelessWidget {
  const FeedPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        const Text(
          'Feed Page',
        ),
        const SizedBox(height: 15,),
        ElevatedButton(
          onPressed: () {
            context.go('/feed/1');
          },
          child: const Text('Go to Feed Detail 1'),
        ),
        const SizedBox(height: 15,),
        ElevatedButton(
          onPressed: () {
            context.go('/feed/2');
          },
          child: const Text('Go to Feed Detail 2'),
        ),
        const Spacer()
      ],
    );
  }
}

class ShopePage extends StatelessWidget {
  const ShopePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Shop Page'),
    );
  }
}

class FeedDetailPage extends StatelessWidget {
  const FeedDetailPage(this.id, {super.key});

  final int id;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Feed Detail Page ID: $id',
      ),
    );
  }
}

